<?php


namespace QI\AddClass\Commands;


interface CommandInterface
{
    // add any action in your implementation
    function execute();
}